# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils import timezone

from django import forms

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.mail import send_mail

from models import Wpis
from models import User
from models import Tag
from models import Grupa

import hashlib


class DodajForm(forms.Form):
    wpis = forms.CharField(max_length=200, min_length=10)
    tagi = forms.CharField(max_length=500)

class LoginForm(forms.Form):
    login = forms.CharField(max_length=100)
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    login = forms.CharField(max_length=100)
    email = forms.EmailField()
    haslo = forms.CharField(max_length=32, widget=forms.PasswordInput)
    nhaslo = forms.CharField(max_length=32, widget=forms.PasswordInput)

class FilterForm(forms.Form):
    by_tag = forms.CharField(max_length=100)

class EditForm(forms.Form):
    tresc = forms.CharField(max_length=500)


class WpisTagi(object):

    def __init__(self, tresc, autor, data, tagi):
        self.tresc = tresc
        self.autor = autor
        self.data = data
        self.tagi = tagi




def dodaj(request):
    form = DodajForm() # An unbound form
    return render(request, 'microblog/dodawanie.html', {
        'forma': form,
        'zalogowany': request.session['zalogowany']})

def login(request):
    forma = LoginForm()
    return render(request, 'microblog/login.html', {
        'forma': forma,
        'zalogowany': request.session['zalogowany']})

def register(request):
    forma = RegisterForm()
    return render(request, 'microblog/register.html', {
        'forma': forma,
        'zalogowany': request.session['zalogowany']})

def doLogin(request):
    forma = LoginForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            haslo = forma.cleaned_data['haslo']
            u = User.objects.filter(login=username, haslo=haslo)


            if u.count() is 1:
                if u[0].aktywowany is False:
                    request.session['zalogowany'] = 'False'
                    request.session['user'] = None
                    return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'not_activated',)))

                request.session['zalogowany'] = 'True'
                request.session['user'] = username
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'login_ok',)))
            else:
                request.session['zalogowany'] = 'False'
                request.session['user'] = None
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad','wrong', )))

    else:
        if request.session['zalogowany'] == 'True':
            return HttpResponseRedirect(reverse('microblog.views.komunikat'))
        else:
            return HttpResponseRedirect(reverse('microblog.views.login'))

def doRegister(request):
    forma = RegisterForm(request.POST)

    if request.method == 'POST':

        if forma.is_valid():
            username = forma.cleaned_data['login']
            email = forma.cleaned_data['email']
            haslo = forma.cleaned_data['haslo']
            nhaslo = forma.cleaned_data['nhaslo']
            ilosc = User.objects.filter(login=username).count()
            if ilosc >= 1:
                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'user_exists',)))
            else:
                if not haslo == nhaslo:
                    return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'pwd_not_match',)))

                token = hashlib.md5(username + email).hexdigest()

                msg = unicode.format(u'Aby aktywowac konto, kliknij w link: http://194.29.175.240/~p8/wsgi/blog/activate/{0}/',
                                     token)
                send_mail('Aktywacja konta na pr0blogu', msg, 'noreply@pr0blog.pl', [email, ], fail_silently=False)

                u = User(login=username, haslo=haslo, email=email, tokenAktywacyjny=token)
                u.save()



                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'register_ok',)))

    else:
        return HttpResponseRedirect(reverse('microblog.views.register'))

def komunikat(request, typ, message):
    tresc = ''
    if message == 'login_ok':
        tresc = 'Zalogowano pomyslnie'
    elif message == 'logout_ok':
        tresc = 'Wylogowano pomyslnie'
    elif message == 'not_logged_in':
        tresc = 'Nie jestes zalogowany'
    elif message == 'wpis_ok':
        tresc = 'Twoj wpis dodano pomyslnie'
    elif message == 'user_exists':
        tresc = 'Taki uzytkownik juz istnieje'
    elif message == 'pwd_not_match':
        tresc = 'Podane hasla nie sa identyczne'
    elif message == 'register_ok':
        tresc = 'Rejestracja poprawna. E-mail aktywacyjny zostal wyslany na podany adres.'
    elif message == 'not_activated':
        tresc = 'Konto nie jest aktywne'
    elif message == 'activated':
        tresc = 'Twoje konto zostalo aktywowane. Mozesz sie zalogowac.'
    elif message == 'edit_ok':
        tresc = 'Edycja postu przebiegla pomyslnie.'
    elif message == 'cant_edit':
        tresc = 'Musi minac conajmniej 10 minut od ostatniej edycji lub dodania wpisu'
    else:
        tresc = 'Cos chyba kombinujesz'

    if typ == 'ok' or typ == 'blad':
        return render(request, 'microblog/komunikat.html',
            {
                'typ': typ,
                'tresc': tresc,
                'zalogowany': request.session['zalogowany'],
            })
    else:
        return render(request, 'microblog/komunikat.html',
            {
                'typ': 'blad',
                'tresc': tresc,
                'zalogowany': request.session['zalogowany'],
            })



def index(request):
    if not request.session.get('zalogowany'):
        request.session['zalogowany'] = 'False'
        request.session['user'] = None

    #sprawdzanie czy zalogowany uzytkownik jest moderatorem
    ile = User.objects.filter(login=request.session['user'], grupy__nazwa='moderator').count()

    return render(request, 'microblog/index.html', {
        'zalogowany': request.session['zalogowany'],
        'user': request.session['user'],
        'moderator': bool(ile),
        'wszystkie_wpisy': Wpis.objects.all().order_by('-data'),
    })

def logout(request):
    if request.session['zalogowany'] == 'True':
        request.session['zalogowany'] = 'False'
        request.session['user'] = None
        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'logout_ok',)))
    else:
        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'not_logged_in',)))



def add(request):
    if request.session['zalogowany'] == 'True':
        if request.method == 'POST': # If the form has been submitted...
            # ContactForm was defined in the the previous section
            form = DodajForm(request.POST) # A form bound to the POST data
            if form.is_valid():
                user = request.session['user']
                tresc = form.cleaned_data['wpis']
                tagi = form.cleaned_data['tagi']
                data = timezone.now()
                w = Wpis(autor=user, tresc=tresc, data=data)
                w.save()

                #obrobka tagow
                splitted = unicode(tagi).split(u' ')

                #sprawdzic czy tag jest w bazie, jesli nie to dodac
                for tag in splitted:
                    if Tag.objects.filter(name=tag).count() is 0:
                        t = Tag(name=tag)
                        t.save()
                        w.tagi.add(t)
                    else:
                        t = Tag.objects.filter(name=tag)[0]
                        w.tagi.add(t)

                return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'wpis_ok',)))

        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'fdsfds',)))

    else:
        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'not_logged_in',)))

def filter(request):
    forma = FilterForm()
    return render(request, 'microblog/filter.html', {
        'forma': forma,
        'zalogowany': request.session['zalogowany']})

def doFilter(request):

    if request.method == 'POST':
        forma = FilterForm(request.POST)

        if forma.is_valid():
            tag = forma.cleaned_data['by_tag']
            return HttpResponseRedirect(reverse('microblog.views.showUserPosts', args=(tag,)))

    return HttpResponseRedirect(reverse('microblog.views.index'))

def showUserPosts(request, tag):
    if not request.session.get('zalogowany'):
        request.session['zalogowany'] = 'False'

    #najpierw pobrac wszystkie wpisy, ktore maja dany tag, jesli nie ma tagu to wszystkie
    if tag != '':
        t = Tag.objects.filter(name=tag)
        po_tagach = Wpis.objects.filter(tagi=t)

    else:
        po_tagach = Wpis.objects.all()


    return render(request, 'microblog/index.html', {
        'zalogowany': request.session['zalogowany'],
        'wszystkie_wpisy': po_tagach.order_by('-data'),
        })

def activate(request, token):
    #sprawdz czy nieaktywowany user z takim tokenem jest w bazie
    u = User.objects.filter(aktywowany=False, tokenAktywacyjny=token)

    if u.count() is 1:
        user = u[0]
        user.aktywowany = True
        user.save()
        return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'activated',)))

    return HttpResponseRedirect(reverse('microblog.views.komunikat'))

def doEdit(request):

    if request.method == 'POST':
        forma = EditForm(request.POST)

        if forma.is_valid():
            #znajdz wpis o danym id
            id = int(request.session['edit_post'])
            nowa_tresc = forma.cleaned_data['tresc']
            wpis = Wpis.objects.filter(id=id)[0]
            wpis.tresc = nowa_tresc
            wpis.dataModyfikacji = timezone.now()
            wpis.autorModyfikacji = request.session['user']
            wpis.save()
            request.session['edit_post'] = ''
            return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('ok', 'edit_ok',)))

    return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'wrong',)))

def edit(request, id):
    #sprawdzic czy zalogowany user jest moderatorem
    ile = User.objects.filter(login=request.session['user'], grupy__nazwa='moderator').count()
    post = Wpis.objects.filter(id=id)[0]

    #jesli jest
    if bool(ile):
        forma = EditForm()
        request.session['edit_post'] = id
        return render(request, 'microblog/edit.html', {
            'forma': forma,
            'zalogowany': request.session['zalogowany']})
    #jesli nie jest
    else:
        if post.dataModyfikacji is not None:
            ostatnia_edycja = post.dataModyfikacji
        else:
            ostatnia_edycja = post.data

        roznica = (timezone.now() - ostatnia_edycja).seconds/60

        if roznica < 10:
            return HttpResponseRedirect(reverse('microblog.views.komunikat', args=('blad', 'cant_edit',)))

        else:
            forma = EditForm()
            request.session['edit_post'] = id
            return render(request, 'microblog/edit.html', {
            'forma': forma,
            'zalogowany': request.session['zalogowany']})
